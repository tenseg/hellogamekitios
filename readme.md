# HelloGameKitiOS

This project is based on the [HelloGameKit watchKit app](https://developer.apple.com/library/content/samplecode/HelloGameKit/Introduction/Intro.html) released by Apple in October 2016. While it was great that Apple finally released a sample app to demonstrate the use of turn based games, I found it frustrating that the game was encumbered by watchOS elements and included no help in how to get a game set up in iTunes Connect for actual gameplay. In fact, I never got that sample running.

This version includes much more robust error checking and reporting than in the original sample. It may be a bit overboard, but the intent of this code is to document working with Game Center and the extra error checking makes the expected outcomes much more evident.

In addition to basic turn-taking, this code demonstrates how to send updates to all players from the current player during the course of a turn (pokes). 

This code is designed to run in iOS. As much of the other complexity as possible has been eliminated, so that you can see how the turn based elements do their thing.

This document was created in July 2017, keep in mind that some of these systems will change over time and these instructions may not be exactly correct for long.

## Setup in iTunes Connect

A GameCenter game runs with severe constraints in the iOS simulator. This game needs some configuration in iTunes Connect to be able to actually run outside the simulator. To make this work for you outside of the simulator, please change the bundle identifier in the general project settings so that it is unique to you.

1. Log into the [Apple Developer Portal](https://developer.apple.com/account/ios/certificate/) and add an App ID that matches your unique bundle identifier. Note that Game Center services will be automatically associated with this identifier.
2. Log into [iTunes Connect](https://itunesconnect.apple.com) and set up a new iOS app using your unique bundle identifier. (It may take a few minutes for the Developer Portal to let iTunes Connect know about your identifier. If it is not on the pop-up list, just wait a few minutes and try again.)
3. In the "Prepare for Submission" section of iTunes Connect for this new app, scroll down and turn on the Game Center switch, then save the changes.

At this point your game should be able to authenticate and use Game Center on an iOS device.
