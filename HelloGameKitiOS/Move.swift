//  Move.swift
//  HelloGameKitiOS
//
//  Created by Eric Celeste on 17.07.12.
//  Copyright © 2017 Tenseg LLC.
//
//  Permission is hereby granted, free of charge,
//  to any person obtaining a copy of this software
//  and associated documentation files (the "Software"),
//  to deal in the Software without restriction,
//  including without limitation the rights to use, copy,
//  modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons
//  to whom the Software is furnished to do so, subject
//  to the following conditions:
//
//  The above copyright notice and this permission notice
//  shall be included in all copies or substantial portions
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.
//
//  This project was based on Apple's 2016 HelloGameKit skeleton.
//  https://developer.apple.com/library/content/samplecode/HelloGameKit

import UIKit

/// Holds a single move in the game.
class Move: NSObject, NSCoding {
    // MARK: Properties
    
    /// The date and time of the move.
    let date: Date
    
    /// The ID of the player who made the move.
    let playerID: String
    
    /// A formatter used to clean up date output.
    private let dateFormatter: DateFormatter = DateFormatter()
    
    /// A textual representation of the move.
    override public var description: String {
        dateFormatter.dateFormat = "DDD-HH-mm-ss"
        return "\n\(dateFormatter.string(from: date)) \(playerID) "
    }
    
    // MARK: Initialization
    
    /// Initialize the move with a player.
    ///
    /// Note that the move assumes that the time of the move
    /// is the time at which the move was initialized.
    ///
    /// - Parameter playerID: The Game Center ID of the player making the move.
    init(playerID: String) {
        self.playerID = playerID
        self.date = Date()
        super.init()
    }
    
    // MARK: NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        date = aDecoder.decodeObject(forKey:"date") as! Date
        playerID = aDecoder.decodeObject(forKey:"playerID") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(date as Date, forKey: "date")
        aCoder.encode(playerID as NSString, forKey: "playerID")
    }
}
