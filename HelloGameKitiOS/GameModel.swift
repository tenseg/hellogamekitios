//  GameModel.swift
//  HelloGameKitiOS
//
//  Created by Eric Celeste on 17.07.12.
//  Copyright © 2017 Tenseg LLC.
//
//  Permission is hereby granted, free of charge,
//  to any person obtaining a copy of this software
//  and associated documentation files (the "Software"),
//  to deal in the Software without restriction,
//  including without limitation the rights to use, copy,
//  modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons
//  to whom the Software is furnished to do so, subject
//  to the following conditions:
//
//  The above copyright notice and this permission notice
//  shall be included in all copies or substantial portions
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.
//
//  This project was based on Apple's 2016 HelloGameKit skeleton.
//  https://developer.apple.com/library/content/samplecode/HelloGameKit

import Foundation
import GameKit

extension GKTurnBasedMatch {
    
    /// The first participant who is not the local player.
    var firstOpponentParticipant: GKTurnBasedParticipant? {
        let localPlayerID = GKLocalPlayer.localPlayer().playerID!
        let opponentParticipant = participants?.first { participant in
            let playerID = participant.player?.playerID
            return playerID != localPlayerID
        }
        
        return opponentParticipant
    }
    
    /// A title for the match.
    var title: String {
        if let opponentPlayer = firstOpponentParticipant?.player {
            return opponentPlayer.alias ?? opponentPlayer.displayName!
        }
        else {
            return "Auto-Match"
        }
    }
    
    /// Some details about the match.
    var detail: String {
        if let message = self.message, !message.isEmpty {
            return message
        }
        switch status {
        case .matching:
            if currentParticipant?.player == GKLocalPlayer.localPlayer() {
                return "Your Turn"
            }
            else {
                return "Finding Player"
            }
            
        case .open:
            if currentParticipant?.player == GKLocalPlayer.localPlayer() {
                return "Your Turn"
            }
            else {
                return "Their Turn"
            }
        case .ended:
            return "Game Over"
            
        default:
            return "<unknown>"
        }
    }
    
    /// An abbreviated version of the match ID for human consumption during debugging.
    var shortID: String {
        guard let id = matchID else {
            return ""
        }
        return id.substring(with: id.startIndex ..< id.characters.index(id.startIndex, offsetBy: 3))
            + "⋯"
            + id.substring(with: id.characters.index(id.endIndex, offsetBy: -3) ..< id.endIndex)
    }
    
    /// A textual representation of some key match information.
    var information: String {
        guard let participants = participants else {
            return "match \(shortID) without participants"
        }
        return "\(title) (\(detail)) match \(shortID) \(participants.count) participants \(participants.map({" 🌏 \($0.player?.playerID ?? "-") \($0.player?.displayName ?? "-")"}).joined(separator: "")) 👉🏽 \(currentParticipant?.player?.playerID ?? "-")"
    }
}

/// Errors that may be passed when working with the game model.
///
/// - pokingOutOfTurnError: The user was poked when it was not their turn.
/// - unarchivingError: Unarchiving data turned up nothing.
public enum GameModelError: Error {
    case pokingOutOfTurnError
    case unarchivingError
}

extension GameModelError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .pokingOutOfTurnError:
            return NSLocalizedString("Attempted to poke during another player's turn.", comment: "A pokingOutOfTurnError")
        case .unarchivingError:
            return NSLocalizedString("Unarchiving data failed.", comment: "An unarchivingError")
        }
    }
}

/// Manages the game itself and interactions with the Game Center match.
class GameModel: NSObject, NSCoding {
    // MARK: Types
    
    /// Holds values used as player identifiers when real identifiers are not available.
    private struct PlayerIdentifiers {
        /// Used when we assume the player is still being matched.
        static let automatch = "automatchID"
        /// Used when we assume the player is the person holding the device.
        static let local = "localID"
    }
    
    // MARK: Properties
    
    /// Array of moves made during the game.
    private(set) var moves = [Move]()
    
    /// Array of player identifiers for participants in the game.
    private var players = [String]()
    
    /// Identifier for the player who is currently taking a turn.
    private(set) var currentPlayerID: String
    
    /// A shortcut for accessing the local player's Game Center identifier.
    var localPlayerID: String? {
        return GKLocalPlayer.localPlayer().playerID
    }
    
    /// True if it is the local player's turn.
    var isLocalPlayerTurn : Bool {
        return localPlayerID == currentPlayerID
    }
    
    /// The Game Center match associated with this game.
    var match: GKTurnBasedMatch? {
        didSet {
            // Whenever the match is changed we syncronize
            // the current player and list of players with the match
            
            if let playerID = match?.currentParticipant?.player?.playerID {
                currentPlayerID = playerID
            }
            else {
                currentPlayerID = PlayerIdentifiers.automatch
            }
            
            players.removeAll() // start with a clean slate of players
            
            guard let participants = match?.participants else {
                return // if there are no participants, then we are done
            }
            
            for participant in participants {
                if let playerID = participant.player?.playerID {
                    players.append(playerID)
                }
                else {
                    players.append(PlayerIdentifiers.automatch)
                }
            }
        }
    }
    
    /// The most recent move made.
    var currentMove: Move? {
        return moves.last
    }
    
    /// Stores the pokes during a turn.
    var pokes = ""
    
    /// A textual representation of the game model state.
    override public var description: String {
        return "\(match?.information ?? "none") moves \(moves.description)\(pokes)"
    }
    
    // MARK: Initialization
    
    override init() {
        if let playerID = GKLocalPlayer.localPlayer().playerID {
            currentPlayerID = playerID
        }
        else {
            currentPlayerID = PlayerIdentifiers.local
        }
        
        players = [currentPlayerID]
        
        super.init()
    }
    
    // MARK: NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        moves = aDecoder.decodeObject(forKey: "moves") as! [Move]
        players = aDecoder.decodeObject(forKey: "players") as! [String]
        pokes = aDecoder.decodeObject(forKey: "pokes") as! String
        currentPlayerID = aDecoder.decodeObject(forKey: "currentPlayerID") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(moves as NSArray, forKey: "moves")
        aCoder.encode(players as NSArray, forKey: "players")
        aCoder.encode(pokes as NSString, forKey: "pokes")
        aCoder.encode(currentPlayerID as NSString, forKey: "currentPlayerID")
    }
    
    // MARK: Factory methods
    
    /// Populate the game model with data found in the given match.
    ///
    /// - Parameters:
    ///   - match: Game Center match from which to populate the model.
    ///   - completionHandler: A closure which is called with
    ///                        a `GameModel` and an `Error`
    ///                        when loading is complete.
    class func loadGameModel(match: GKTurnBasedMatch, completionHandler: @escaping (GameModel?, Error?) -> Void) {
        print("***** loading matchData")
        match.loadMatchData { data, error in
            guard error == nil else {
                print("***** matchData load failed")
                return completionHandler(nil, error)
            }
            print("***** matchData load succeeded")
            var model: GameModel? = nil
            
            // If we have data use it otherwise make a new model.
            if let data = data, data.count > 0 {
                model = NSKeyedUnarchiver.unarchiveObject(with: data) as? GameModel
                guard model != nil else {
                    print("***** matchData unarchive failed")
                    return completionHandler(nil, GameModelError.unarchivingError)
                }
            } else {  // this is a new match so make a model
                model = GameModel()
            }
            model?.match = match
            completionHandler(model, nil)
        }
    }
    
    // MARK: Making Moves and taking turns
    
    /// Adds a move to the array of moves and saves new game state to Game Center.
    ///
    /// - Parameter completionHandler: A closure which is called with
    ///                                a `Move` and an `Error`
    ///                                when the game state has been saved.
    /// - Returns: The move.
    func addMove(completionHandler: @escaping (Move?, Error?) -> Void) {
        let move = Move(playerID: currentPlayerID)
        moves.append(move)
        save { error in
            completionHandler(move, error)
        }
    }
    
    /// Generates an array of the next participants in the game.
    ///
    /// - Returns: An array of the next participants in the game.
    func nextParticipants() -> [GKTurnBasedParticipant] {
        guard let participants = match?.participants else { fatalError("No participants.") }
        
        // Compute the index of the participant to whom we wish to pass the turn to.
        var nextIndex: Int? = nil
        if let index = players.index(of: currentPlayerID) {
            var offset = 1
            while nextIndex == nil && offset <= players.count {
                nextIndex = (index + offset) % players.count
                if participants[nextIndex!].status == .done {
                    nextIndex = nil
                    offset += 1
                }
            }
        }
        
        guard let next = nextIndex else {
            print("***** no valid next player found")
            return []
        }
        
        // Build next participants list including all participants
        // starting with the new turn taker.
        // The current turn holder will be included at the
        // end so if the none of the players take their turn
        // the turn will eventually return to the current turn holder.
        var nextParticipants = participants[next..<participants.count]
        nextParticipants += participants[0..<next]
        
        let nextParticipant = nextParticipants.first
        
        // Assume players are being matched until proven otherwise.
        var nextPlayerID = PlayerIdentifiers.automatch
        if let playerID = nextParticipant?.player?.playerID {
            nextPlayerID = playerID
        }
        
        // Now that we have figured our our next player
        // we update our currentPlayerID to be that player
        currentPlayerID = nextPlayerID
        
        return Array(nextParticipants)
    }
    
    /// End the turn.
    ///
    /// - Parameter completionHandler: A closure which is called with
    ///                                an `Error` when the turn has ended.
    func endTurn(completionHandler: @escaping (Error?) -> Void) {
        let next = nextParticipants()
        pokes = "" // clear these exchanges at the end of each turn
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        match?.endTurn(withNextParticipants: next, turnTimeout: 600, match: data) { error in
            if let error = error {
                print("***** error ending turn \(error)")
            }
            completionHandler(error)
        }
    }
    
    /// Quit the game.
    ///
    /// - Parameters:
    ///   - inTurn: True if the local player is also taking a turn.
    ///   - completionHandler: A closure which is called with
    ///                        an `Error` when the quit is acknowledged.
    func quit(completionHandler: @escaping (Error?) -> Void) {
        if isLocalPlayerTurn {
            let next = nextParticipants()
            let data = NSKeyedArchiver.archivedData(withRootObject: self)
            match?.participantQuitInTurn(with: .quit, nextParticipants: next, turnTimeout: 600, match: data) { error in
                completionHandler(error)
            }
        } else {
            match?.participantQuitOutOfTurn(with: .quit) { error in
                completionHandler(error)
            }
        }
    }
    
    /// Checks to see if the current player is the only player still playing,
    /// and if so awards the win to the current player.
    func checkForWin(completionHandler: @escaping (Bool, Error?) -> Void) {
        guard let stillPlaying = match?.participants?.filter({ $0.matchOutcome == .none }),
            stillPlaying.count == 1,
            stillPlaying[0].player?.playerID == currentPlayerID
            else {
                return completionHandler(false, nil)
        }
        
        stillPlaying[0].matchOutcome = .won
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        
        match?.endMatchInTurn(withMatch: data) { error in
            print("***** match ended")
            completionHandler(true, error)
        }
    }
    
    // MARK: Match updating
    
    /// Encodes the game model data and sends it to the Game Center match.
    ///
    /// - Parameters:
    ///   - completionHandler: A closure which is called with
    ///                        an `Error` when saving is complete.
    func save(completionHandler: @escaping (Error?) -> Void) {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        print("***** saving matchData")
        
        match?.saveCurrentTurn(withMatch: data) { error in
            if let error = error {
                print("***** save failed for matchData with error: \(error)")
            }
            else {
                print("***** save complete for matchData")
            }
            completionHandler(error)
        }
    }
    
    /// Refreshes the match data in the model with data from Game Center.
    ///
    /// - Parameter completionHandler: A closure which is called with
    ///                                a `GKTurnBasedMatch` and and `Error`
    ///                                after the match has been loaded.
    func reloadMatch(completionHandler: @escaping (GKTurnBasedMatch?, Error?) -> Void) {
        GKTurnBasedMatch.load(withID: self.match?.matchID ?? "") { match, error in
            if let error = error {
                print("***** error reloading match \(error)")
            } else {
                print("***** reloaded match \(match?.description ?? "empty")")
                self.match = match
            }
            completionHandler(match, error)
        }
    }
    
    // MARK: Updates
    
    /// Updates game state for other players. Can only be done by player during their turn.
    ///
    /// - Parameter completionHandler: A closure which is called with
    ///                                an `Error` when poke is completed.
    func poke(completionHandler: @escaping (Error?) -> Void) {
        let pokeString = "Poke!"
        guard isLocalPlayerTurn else {
            return completionHandler(GameModelError.pokingOutOfTurnError)
        }
        sharePoke(pokeString, withResolvedExchanges: [], completionHandler: completionHandler)
    }
    
    /// Update other players by saving the merged match including the new poke.
    ///
    /// - Parameters:
    ///   - pokeString: The text to be added to our pokes.
    ///   - withResolvedExchanges: An array of exchanges that are
    ///                            meant to be resolved by this update.
    ///   - completionHandler: A closure which is called with
    ///                        an `Error` when saving is completed.
    func sharePoke(_ pokeString: String, withResolvedExchanges exchanges: [GKTurnBasedExchange], completionHandler: @escaping (Error?) -> Void) {
        pokes = "\(pokes) \(pokeString)"
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        match?.saveMergedMatch(data, withResolvedExchanges: exchanges, completionHandler: completionHandler)
    }
    
}


