//  ViewController.swift
//  HelloGameKitiOS
//
//  Created by Eric Celeste on 17.07.12.
//  Copyright © 2017 Tenseg LLC.
//
//  Permission is hereby granted, free of charge,
//  to any person obtaining a copy of this software
//  and associated documentation files (the "Software"),
//  to deal in the Software without restriction,
//  including without limitation the rights to use, copy,
//  modify, merge, publish, distribute, sublicense, and/or
//  sell copies of the Software, and to permit persons
//  to whom the Software is furnished to do so, subject
//  to the following conditions:
//
//  The above copyright notice and this permission notice
//  shall be included in all copies or substantial portions
//  the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//  DEALINGS IN THE SOFTWARE.
//
//  This project was based on Apple's 2016 HelloGameKit skeleton.
//  https://developer.apple.com/library/content/samplecode/HelloGameKit

import UIKit
import Foundation
import GameKit

class ViewController: UIViewController, GKTurnBasedMatchmakerViewControllerDelegate, GKLocalPlayerListener {
    
    // MARK: Setup and teardown
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.debugButton.isEnabled = true;
        self.startButton.isEnabled = true;
        
        let localPlayer = GKLocalPlayer.localPlayer()
        print("***** authenticating local player")
        
        localPlayer.authenticateHandler = { controller, error in
            if let error = error {
                print("***** auth failed \(error)")
            } else if let controller = controller {
                print("***** presenting authentication controller")
                self.present(controller, animated: true, completion: nil)
            } else {
                print("***** auth successful \(localPlayer)")
                localPlayer.register(self) // this ensures we get notifications of game state changes
            }
            self.updateUI()
        }
        
        model = GameModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Outlets and Actions
    
    @IBOutlet weak var startButton: UIButton!
    @IBAction func startButtonTapped(_ sender: Any) {
        thereHasBeenAnError = false
        
        let request = GKMatchRequest()
        request.minPlayers = 2
        request.maxPlayers = 3
        
        let matchmakerViewController = GKTurnBasedMatchmakerViewController(matchRequest: request)
        matchmakerViewController.turnBasedMatchmakerDelegate = self
        present(matchmakerViewController, animated: true, completion: nil)
    }
    
    @IBOutlet weak var debugButton: UIButton!
    @IBAction func debugButtonTapped(_ sender: Any) {
        // just attach a breakpoint to this print line to view details
        // for example, maybe a Debugger Command "po model"
        // with "automatically continue" checked
        print("***** debug button tapped")
    }
    
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var gameTextView: UITextView!
    
    @IBOutlet weak var turnButton: UIButton!
    @IBAction func turnButtonTapped(_ sender: Any) {
        if model?.match != nil {
            turnButton.isEnabled = false; // avoid tapping multiple turns
            model?.addMove() { move, error in
                guard self.isNotError(error, during: "recording of move") else { return }
                self.model?.endTurn() { error in
                    guard self.isNotError(error, during: "end of turn") else { return }
                    self.model?.reloadMatch() { match, error in
                        guard self.isNotError(error, during: "reload of match after turn ended") else { return }
                        self.updateUI()
                    }
                }
            }
        }
    }
    
    @IBOutlet weak var resignButton: UIButton!
    @IBAction func resignButtonTapped(_ sender: Any) {
        turnButton.isEnabled = false
        resignButton.isEnabled = false
        model?.quit() { error in
            guard self.isNotError(error, during: "resignation from game") else { return }
            self.updateUI()
        }
    }
    
    @IBOutlet weak var pokeButton: UIButton!
    @IBAction func pokeButtonTapped(_ sender: Any) {
        model?.poke() { error in
            guard self.isNotError(error, during: "poke") else { return }
            self.updateUI()
        }
    }
    
    // MARK: Properties
    
    /// Holds our game model.
    var model: GameModel? {
        didSet {
            updateUI()
        }
    }
    
    /// True if the local player has already been authenticated with Game Center.
    var isLocalPlayerAuthenticated: Bool {
        return GKLocalPlayer.localPlayer().isAuthenticated
    }
    
    /// True if an error has been thrown along the way.
    var thereHasBeenAnError: Bool = false
    
    // MARK: Updating the interface
    
    /// Update the state of all the buttons and text on the screen.
    private func updateUI() {
        
        guard isLocalPlayerAuthenticated else {
            startButton.isEnabled = false
            startButton.setTitle("Login to Game Center", for: .normal)
            self.gameLabel.text = "Waiting for Game Center Login"
            self.gameTextView.text = "Sorry, this game cannot work without a Game Center login. Please log in to Game Center in the Settings app, then restart this game."
            return
        }
        
        startButton.isEnabled = true
        startButton.setTitle("Start a Game", for: .normal)
        
        turnButton.isEnabled = false
        resignButton.isEnabled = false
        pokeButton.isEnabled = false
        
        guard !thereHasBeenAnError else {
            gameLabel.text = "Oops, please restart"
            return
        }
        guard let model = model else {
            gameTextView.text = "No model, which is odd. Start a new game above."
            return
        }
        
        let localPlayer = GKLocalPlayer.localPlayer()
        
        guard let match = model.match else {
            gameTextView.text = "No game to show you yet. Start a game above."
            gameLabel.text = "Welcome, \(localPlayer.alias ?? "whoever you are")"
            return
        }
        
        if model.isLocalPlayerTurn {
            turnButton.isEnabled = true
            pokeButton.isEnabled = true
            gameLabel.text = "Your turn, \(localPlayer.alias ?? "whoever you are")"
        } else {
            let currentPlayer = model.match?.currentParticipant?.player
            gameLabel.text = "Waiting for \(currentPlayer?.displayName ?? currentPlayer?.alias ?? "someone")"
        }
        
        gameTextView.text = "\(model.description)"
        
        let locals = match.participants?.filter({ $0.player?.playerID == localPlayer.playerID })
        if locals?[0].matchOutcome == .quit {
            gameLabel.text = "You quit"
        } else if locals?[0].matchOutcome == .won {
            gameLabel.text = "You won"
        } else {
            resignButton.isEnabled = true
        }
    }
    
    /// Checks whether the error passed in was an actual error and reports that error through the UI.
    /// Lets the rest of the world know about the error by changing state of `thereHasBeenAnError`.
    ///
    /// - Parameters:
    ///   - error: The error being reviewed.
    ///   - during: A textual description of the phase of the program during which the error was thrown.
    /// - Returns: True if the error was an actual error.
    private func isNotError(_ error: Error?, during: String? = nil) -> Bool {
        if let error = error {
            print("***** caught error \(error)")
            gameTextView.text.append("\((during != nil) ? "\n\nDuring \(during!): " : "")\(error.localizedDescription)\n\nPlease restart the game above.")
            thereHasBeenAnError = true
            updateUI()
        }
        return !thereHasBeenAnError
    }
    
    // MARK: Matchmaker Delegate
    
    open func turnBasedMatchmakerViewControllerWasCancelled(_ viewController: GKTurnBasedMatchmakerViewController) {
        print("***** matchmaking cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    open func turnBasedMatchmakerViewController(_ viewController: GKTurnBasedMatchmakerViewController, didFailWithError error: Error) {
        print("***** matchmaking error \(error)")
        _ = isNotError(error, during: "matchmaking")
        dismiss(animated: true, completion: nil)
        updateUI()
    }
    
    // MARK: Player Listener
    
    ///    Activates the player's turn.
    open func player(_ player: GKPlayer, receivedTurnEventFor match: GKTurnBasedMatch, didBecomeActive: Bool) {
        print("***** player received turn event \(didBecomeActive ? "and became active for" : "for") match \(match.shortID)")
        dismiss(animated: true, completion: nil)
        
        if didBecomeActive {
            // This event activated the application. This means that the user
            // tapped on the notification banner and wants to see or play this
            // match now.
            print("***** attaching the model to this match")
            model?.match = match
        } else if model?.match?.matchID == match.matchID {
            // This is the match the user is currently playing,
            // laoding the game model below iwll update to show the latest state
            print("***** refreshing data for this match")
        } else if match.currentParticipant?.player?.playerID == model?.localPlayerID {
            // It became the player's turn in a different match,
            // prompt the player to switch to the new match
            print("***** ignoring player's turn in match \(match.shortID)")
            gameTextView.text.append("\n\nFYI, it is your turn in another match.")
            return
        } else {
            // Something has changed in another match,
            // but not sure what.
            print("***** ignoring new data for match \(match.shortID)")
            gameTextView.text.append("\n\nFYI, something has changed in another match.")
            return
        }
        
        print("***** loading match")
        GameModel.loadGameModel(match: match) { model, error in
            guard self.isNotError(error, during: "model loading") else { return }
            guard let model = model else {
                print("***** no model created")
                self.gameLabel.text = "Error setting up game"
                self.thereHasBeenAnError = true
                return
            }
            print("***** match load succeeded")
            self.model = model
            self.model?.checkForWin() { won, error in
                guard self.isNotError(error, during: "end match request") else { return }
                if won {
                    self.gameLabel.text = "You won!"
                    self.turnButton.isEnabled = false
                    self.resignButton.isEnabled = false
                }
            }
        }
    }
    
    ///    Indicates that the current player wants to quit the current match.
    open func player(_ player: GKPlayer, wantsToQuitMatch match: GKTurnBasedMatch) {
        print("***** player wants to quit match \(match.shortID) NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
    
    ///    Called when the match has ended.
    open func player(_ player: GKPlayer, matchEnded match: GKTurnBasedMatch) {
        print("***** player ended match \(match.shortID) NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
    
    ///    Initiates a match from Game Center with the requested players.
    open func player(_ player: GKPlayer, didRequestMatchWithOtherPlayers playersToInvite: [GKPlayer]) {
        print("***** player requested match with others NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
    
    ///    Called when the exchange is cancelled by the sender.
    open func player(_ player: GKPlayer, receivedExchangeCancellation exchange: GKTurnBasedExchange, for match: GKTurnBasedMatch) {
        print("***** player received exchange cancellation for match \(match.shortID) NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
    
    ///    Called when the exchange is completed.
    open func player(_ player: GKPlayer, receivedExchangeReplies replies: [GKTurnBasedExchangeReply], forCompletedExchange exchange: GKTurnBasedExchange, for match: GKTurnBasedMatch) {
        print("***** player completed exchange for match \(match.shortID) NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
    
    ///    Called when a player receives an exchange request from another player.
    open func player(_ player: GKPlayer, receivedExchangeRequest exchange: GKTurnBasedExchange, for match: GKTurnBasedMatch) {
        print("***** player received exchange for match \(match.shortID) NOT IMPLEMENTED")
        dismiss(animated: true, completion: nil)
    }
}


